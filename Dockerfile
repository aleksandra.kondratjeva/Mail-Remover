# Frontend build
FROM node:16 as frontend-builder
WORKDIR /app/mail-remover
COPY mail-remover/package*.json ./
RUN npm install
COPY mail-remover .
RUN npm run build

# Backend build
FROM mcr.microsoft.com/dotnet/sdk:6.0 as backend-builder
WORKDIR /app/MailRemoverAPI/MailRemoverAPI
COPY MailRemoverAPI/MailRemoverAPI/*.csproj ./
RUN dotnet restore
COPY MailRemoverAPI/MailRemoverAPI .
RUN dotnet build

# Tests
FROM mcr.microsoft.com/dotnet/sdk:6.0 as tester
WORKDIR /app/MailRemoverAPI
COPY MailRemoverAPI .
RUN dotnet test MailRemoverAPI/UnitTests1
RUN dotnet test MailRemoverAPI/AllIntegratinTests

# Deploy
FROM mcr.microsoft.com/dotnet/sdk:6.0 as deployer
WORKDIR /app/MailRemoverAPI/MailRemoverAPI
COPY --from=backend-builder /app/MailRemoverAPI/MailRemoverAPI/bin/Release/net6.0/publish ./
RUN apt-get update && apt-get -y install azure-cli
RUN az login --service-principal -u mail-remover\$mail-remover -p ${DEPLOYMENT_USER_PASSWORD} --tenant ${DIRECTORY_ID}
RUN az webapp deployment source config-zip --resource-group mail-remover_group --name mail-remover --src ./MailRemoverAPI.zip

# Final image
FROM deployer
